<?php

namespace App\Services;

use App\Models\Movie;
use App\Models\Quality;
use Goutte\Client;
use Illuminate\Support\Facades\Http;
use Symfony\Component\DomCrawler\Crawler;
use Weidner\Goutte\GoutteFacade;

class ZoneTelechargementService {
    private static $base_url = 'https://www2.zone-telechargement.monster';

    /**
     * @param String $query
     * @param int $year
     */
    public static function search(String $query, int $year)
    {
        $client = new Client();

        $options = [
            "do"=> "search",
            "subaction"=> "search",
            "catlist" => [2],
            "story"=> $query
        ];
        $crawler = $client->request("POST", self::$base_url.'/index.php?do=search', $options);
        $results = $crawler->filter('.mov');
        // Foreach result
        $links = $results->each(function ($result) use($query, $year) {
            $yearResult = $result->filter('.mov-m a')->last();
            $titleResult = $result->filter('.mov-t')->last();
            if(strtolower($query) == strtolower($titleResult->text()) && $year == $yearResult->text()){
                return $titleResult->link()->getUri();
            }
        });
        return $links;
    }

    /**
     * @param String $url
     * @return Movie
     */
    public static function getMovieWithQualities(String $url)
    {
        $client = new Client();
        $crawler = $client->request("GET", $url);
        $tmdbUrl = $crawler->filter('ul.ul_detail li .field_full + a[target=_blank]')->first()->text();
        preg_match('/^(?:https?:\/\/)?(?:[^@\n]+@)?(?:www\.)?([^:\/\n?]+)\/movie\/(\d+)/', $tmdbUrl, $tmdbMatches);
        if(count($tmdbMatches) == 3 && $tmdbMatches[1] == "themoviedb.org") {
            $tmdbId = $tmdbMatches[2];
        } else {
            return null;
        }

        $movie = Movie::firstOrCreate(['tmdb_id' => $tmdbId]);
        if(!$movie->exists) $movie->save();

        // GET Qualities
        $qualitiesHtml = $crawler->filter('div.otherversions a');
        $qualities = $qualitiesHtml->each(function (Crawler $quality) {
            // Pattern:  QUALITY (LANGUAGE)
            preg_match('/^(.*)\s\(([^)]+)\)$/', $quality->text(), $qualityMatches);
            if(count($qualityMatches) == 3) {
                return Quality::firstOrNew([
                    'quality' => $qualityMatches[1],
                    'language' => $qualityMatches[2],
                    'url' => $quality->link()->getUri()
                ]);
            }
        });
        // current quality
        $qualityHtml = $crawler->filter('div[align=center] h2 + div')->first()->text();
        preg_match('/^Qualité\s(.*)\s\|\s(.*)$/', $qualityHtml, $qualityMatches);
        if(count($qualityMatches) == 3) {
            $qualities[] = Quality::firstOrNew([
                'quality' => $qualityMatches[1],
                'language' => $qualityMatches[2],
                'url' => $url
            ]);
        }
        $movie->qualities()->saveMany($qualities);
        $movie->save();
        dd($movie);
        return $movie;
    }
}
