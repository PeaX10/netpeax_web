<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Quality extends Model
{
    protected $fillable = ['quality', 'language', 'url'];

    public function qualitable()
    {
        return $this->morphTo();
    }

    public function links(){
        return $this->hasMany(Link::class);
    }
}
