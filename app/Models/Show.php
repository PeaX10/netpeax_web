<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Show extends Model
{
    public function qualities(){
        return $this->morphMany(Quality::class, 'qualitable');
    }
}
