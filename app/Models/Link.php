<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    public function quality() {
        $this->belongsTo(Quality::class);
    }
}
