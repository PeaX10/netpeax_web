<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('links', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('hoster_id');
            $table->unsignedBigInteger('file_size')->nullable();
            $table->text('download_link')->nullable();
            $table->text('streaming_link')->nullable();
            $table->timestamps();

            $table->foreign('hoster_id')->references('id')->on('hosters');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('links');
    }
}
